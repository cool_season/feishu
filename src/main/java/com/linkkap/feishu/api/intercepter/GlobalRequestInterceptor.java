package com.linkkap.feishu.api.intercepter;



import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.linkkap.feishu.api.controller.robot.FeiShuRobotController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GlobalRequestInterceptor implements HandlerInterceptor {
	@Override
	public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object arg2, Exception e)
            throws Exception {  
    }  

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object arg2, ModelAndView arg3) throws Exception {
    }
	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		//这里获取到了数据后，后面的请求就拿不到数据了
		ServletInputStream inputStream = request.getInputStream();
		StringBuilder content = new StringBuilder();
		byte[] b = new byte[1024];
		int lens = -1;
		while ((lens = inputStream.read(b)) > 0) {
			content.append(new String(b, 0, lens));
		}
		String strcont = content.toString();// 内容	
		log.debug("请求的数据："+strcont);
		
		return true;
    }	
}
