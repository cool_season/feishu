package com.linkkap.feishu.api.msg.richtext;



import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huang_qiu_sheng
 */
public class RichTextContent {
    private final JSONObject info;
    private List<List<JSONObject>> content;

    public RichTextContent(String title){
        info = new JSONObject();
        info.put("title", title);
    }
    public RichTextContent addTextLine(List<JSONObject> line){
        if(content==null){
            content = new ArrayList<>();
        }
        content.add(line);
        return this;
    }
    public JSONObject done(){
        info.put("content", content);
        return info;
    }
}
