package com.linkkap.feishu.api.msg.richtext;


import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huang_qiu_sheng
 */
public class RichTextLine{
    private final List<JSONObject> line;

    public RichTextLine() {
        this.line = new ArrayList<>();
    }
    public RichTextLine addText(String text){
        JSONObject trunk = new JSONObject();
        trunk.put("tag", "text");
        trunk.put("text", text);
        line.add(trunk);
        return this;
    }
    public RichTextLine addText(String text, boolean unEscape){
        JSONObject trunk = new JSONObject();
        trunk.put("tag", "text");
        trunk.put("text", text);
        trunk.put("un_escape", unEscape);
        line.add(trunk);
        return this;
    }
    public RichTextLine addUrl(String text, String url){
        JSONObject trunk = new JSONObject();
        trunk.put("tag", "a");
        trunk.put("text", text);
        trunk.put("href", url);
        line.add(trunk);
        return this;
    }
    public RichTextLine addUrl(String text, String url, boolean unEscape){
        JSONObject trunk = new JSONObject();
        trunk.put("tag", "a");
        trunk.put("text", text);
        trunk.put("href", url);
        trunk.put("un_escape", unEscape);
        line.add(trunk);
        return this;
    }
    public RichTextLine addAtUser(String userId){
        JSONObject trunk = new JSONObject();
        trunk.put("tag", "at");
        trunk.put("user_id", userId);
        line.add(trunk);
        return this;
    }

    public RichTextLine addImg(String imgKey, int width, int height){
        JSONObject trunk = new JSONObject();
        trunk.put("tag", "img");
        trunk.put("image_key", imgKey);
        trunk.put("width", width);
        trunk.put("height", height);
        line.add(trunk);
        return this;
    }
    public List<JSONObject> done(){
        return line;
    }
}
