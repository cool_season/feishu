package com.linkkap.feishu.api.msg.richtext;


import com.alibaba.fastjson.JSONObject;

/**
 * @author huang_qiu_sheng
 */
public class RichText {
    private final JSONObject responseBody;
    private final JSONObject post;

    public RichText(){
        responseBody = new JSONObject();
        responseBody.put("msg_type", "post");
        post = new JSONObject();
    }

    public RichText addOpenId(String openId){
        responseBody.put("open_id", openId);
        return this;
    }
    public RichText addChatId(String chatId){
        responseBody.put("chat_id", chatId);
        return this;
    }
    public RichText addUserId(String userId){
        responseBody.put("user_id", userId);
        return this;
    }
    public RichText addEmail(String email){
        responseBody.put("email", email);
        return this;
    }
    public RichText addContent(EncodingEnum encodingEnum, JSONObject content){
        post.put(encodingEnum.getEncoding(), content);
        return this;
    }

    public JSONObject done(){
        JSONObject content = new JSONObject();
        content.put("post", post);
        responseBody.put("content", content);
        return responseBody;
    }
}


