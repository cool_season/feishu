package com.linkkap.feishu.api.msg;

import com.alibaba.fastjson.JSONObject;

import java.util.List;
/**
 * @author huang_qiu_sheng
 * 用于构造批量发送文本
 * */
public class BatchText {
    private JSONObject batchText = new JSONObject();
    public BatchText(){
        batchText.put("msg_type", "text");
    }
    public BatchText addDepartmentIds(List<String> deptIds){
        batchText.put("department_ids", deptIds);
        return this;
    }
    public BatchText addOpenIds(List<String> openIds){
        batchText.put("open_ids", openIds);
        return this;
    }
    public BatchText addUserIds(List<String> userIds){
        batchText.put("user_ids", userIds);
        return this;
    }
    public BatchText addText(String text){
        batchText.put("content", new JSONObject().put("text", text));
        return this;
    }
    public JSONObject done(){
        return this.batchText;
    }
}
