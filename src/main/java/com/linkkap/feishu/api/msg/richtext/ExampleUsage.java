package com.linkkap.feishu.api.msg.richtext;

/**
 * @author huang_qiu_sheng
 */
public class ExampleUsage {
    public static void main(String[] args) {
        RichText richText = new RichText();

        richText.addOpenId("1122");

        RichTextContent content = new RichTextContent("欢迎使用链股");
        content.addTextLine(new RichTextLine()
                .addText("链股是一个一站式股权激励智能平台应用，在链股机器人中你可以接收：").done());
        content.addTextLine(new RichTextLine().addText("- 期权授予签字").done());
        content.addTextLine(new RichTextLine().addText("- 期权成熟消息").done());
        content.addTextLine(new RichTextLine().addText("- 期权行权签字").done());
        content.addTextLine(new RichTextLine().addText("如果你有股书账号，请在链股SaaS系统-技术支持 反馈，告知需绑定飞书，我们会及时帮你绑定").done());
        content.addTextLine(new RichTextLine().addText("如果你没有链股账号，请点此 了解更多 或 ").addUrl("直接注册", "https://bytedance.com").done());
        content.addTextLine(new RichTextLine().addText("更多操作请参见：帮助文档").addUrl("百度", "https://www.baidu.com").done());
        content.addTextLine(new RichTextLine().addText("也可以随时反馈：在线反馈").done());

        richText.addContent(EncodingEnum.ZH_CN, content.done());

        System.out.println(richText.done());
    }
}
