package com.linkkap.feishu.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.linkkap.feishu.api.result.ApiResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author  465087012@qq.com
 * @program feishu
 * @create  2021-03-03 18:02
 * @description
 **/
@Slf4j
@RestController
@Api(value = "心跳接口", tags = {"心跳接口"})
public class HealthController {

    @GetMapping("/health.do")
    @ApiOperation(value = "心跳接口", notes = "心跳接口")
    public ApiResult<Object> health() {
    	log.debug("访问health.do接口");
        return ApiResult.SUCCESS;
    }

}

