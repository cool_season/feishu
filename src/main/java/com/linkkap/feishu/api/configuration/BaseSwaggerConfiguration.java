package com.linkkap.feishu.api.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


public class BaseSwaggerConfiguration {
    public Docket defaultApi() {
        List<ResponseMessage> responseMessageList = new ArrayList<>();
        responseMessageList.add(new ResponseMessageBuilder().code(-1).message("系统业务错误，请结合响应信息进行排查业务bug").responseModel(new ModelRef("ApiResult")).build());
        responseMessageList.add(new ResponseMessageBuilder().code(500).message("系统错误异常，请联系后台开发排查代码bug").responseModel(new ModelRef("ApiResult")).build());
        responseMessageList.add(new ResponseMessageBuilder().code(400).message("数据不存在，请传入正确的值，或者结合响应信息联系后台开发排查数据绑定上的空指针").responseModel(new ModelRef("ApiResult")).build());
        responseMessageList.add(new ResponseMessageBuilder().code(401).message("参数验证出错，请传入符合文档要求的参数").responseModel(new ModelRef("ApiResult")).build());
        responseMessageList.add(new ResponseMessageBuilder().code(402).message("请勿重复请求接口").responseModel(new ModelRef("ApiResult")).build());

        return new Docket(DocumentationType.SWAGGER_2)
                .globalResponseMessage(RequestMethod.GET, responseMessageList)
                .globalResponseMessage(RequestMethod.POST, responseMessageList)
                .globalResponseMessage(RequestMethod.PUT, responseMessageList)
                .globalResponseMessage(RequestMethod.DELETE, responseMessageList);
    }
}
