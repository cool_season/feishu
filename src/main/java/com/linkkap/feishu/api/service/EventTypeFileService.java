package com.linkkap.feishu.api.service;

/**
 * @author huang_qiu_sheng
 * @date 3/12/2021 6:30 PM
 */
public interface EventTypeFileService {
    /**
     *
     * @param key
	 * @param value
     * @return void
     * @author huang_qiu_sheng
     * @date 3/12/2021 6:30 PM
     */
    void saveStringByKey(String key, String value);
    /**
     *
     * @param key
     * @return java.lang.String
     * @author huang_qiu_sheng
     * @date 3/12/2021 6:30 PM
     */
    String getStringByKey(String key);
    /**
     * @param key
     * @return void
     * @author huang_qiu_sheng
     * @date 3/12/2021 6:30 PM
     */
    void removeStringByKey(String key);
}
