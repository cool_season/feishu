package com.linkkap.feishu.api.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.FileWriter;
import com.linkkap.feishu.api.service.EventTypeFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author huang_qiu_sheng
 * 主要用于从配置文件中获取数据
 * */
@Service("eventTypeFileService")
@Slf4j
public class EventTypeFileServiceImpl implements EventTypeFileService {
    private final static String EVENT_TYPE_PREFIX = "EventRollbackType";

    private String constructKey(String key){
        return "msg/"+EVENT_TYPE_PREFIX+"-"+key+".json";
    }
    @Override
    public void saveStringByKey(String key, String value) {
        FileWriter writer = new FileWriter(constructKey(key));
        log.info("saveStringByKey: {}", writer.getFile().getAbsoluteFile());
        writer.write(value);
    }

    @Override
    public String getStringByKey(String key) {
        FileReader reader = new FileReader(constructKey(key));
        log.info("getStringByKey: {}", reader.getFile().getAbsoluteFile());
        return reader.readString();
    }

    @Override
    public void removeStringByKey(String key) {
        log.info("removeStringByKey: {}", constructKey(key));
        FileUtil.del(constructKey(key));
    }
}
